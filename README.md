Dear Participant,

Coding Challenge day 6
Kindly go through the problem statement given below carefully and attempt the Challenge and Please submit if you want us to review

Thanks,
Program Office.
# Todo Manager Sprint 6

## Problem Statement
Todo Manager is an application which can Manage our Tasks and keep track of our Tasks.
- Create database `todotaskdb` in Mysql 
- Create table `registration` with 3 columns ie.  id , username , pswd ;
- Create a JDBC Connection.
- User will able to Register.
- User will able to Login with registered details.

## Instructions:-

- Please refactor the code created in spring 5.
- Estimated completion time of the challenge will be 2hr.
- Make sure you're uploading the code on Olympus.
