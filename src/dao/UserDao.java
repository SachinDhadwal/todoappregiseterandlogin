package dao;


import model.User;

import java.util.ArrayList;

public interface UserDao {
    ArrayList<User> users = new ArrayList<>();

    User isValidUser(String name, String password);

    void listAssignedTasks(String name);

    User userFinder(String username);
}
