package daoImpl;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// This class will return the current Date in particular format 'DD-MM-YYYY HH:mm:ss'
public class Date {

    public String dateFormatter() {
        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY HH:mm:ss");
        String formattedDateTime = currentTime.format(formatter);
        return formattedDateTime;
    }

}
