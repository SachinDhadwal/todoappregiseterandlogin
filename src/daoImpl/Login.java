package daoImpl;

import dao.UserDao;
import model.User;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login {

    public Login() {
        try{
            Class.forName(AppConst.driverName);
            AppConst.connection = DriverManager.getConnection(AppConst.db_URL,AppConst.db_userName,AppConst.db_password);

        }
        catch (ClassNotFoundException | SQLException exception) {
            exception.printStackTrace();

        }
    }

    public void doLogin(String username, String password) throws SQLException {
        AppConst.statement = AppConst.connection.prepareStatement("select username, password from registration where username=? and password=?");
        AppConst.statement.setString(1,username.toLowerCase());
        AppConst.statement.setString(2,password.toLowerCase());
        ResultSet resultSet = AppConst.statement.executeQuery();
        if(resultSet.next()) {
            System.out.println("Hii"+username +"Welcome to TodoAPP");
            UserDao.users.add(new User(username,password));
        }
        else {
            System.out.println("Username password does not exit");
            System.exit(0);
        }
    }
}
