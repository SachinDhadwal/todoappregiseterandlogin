package daoImpl;

import model.User;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Register {

    User user;

    public Register() {
        try{
            Class.forName(AppConst.driverName);
            AppConst.connection = DriverManager.getConnection(AppConst.db_URL,AppConst.db_userName,AppConst.db_password);

        }
        catch (ClassNotFoundException |SQLException exception) {
            exception.printStackTrace();

        }

    }

    public void doRegister(String userName, String pswd) throws SQLException {
      String query = "insert into registration(username, password) values(?,?)";
        AppConst.statement = AppConst.connection.prepareStatement(query);
      user = new User(userName,pswd);
      AppConst.statement.setString(1,user.getName());
      AppConst.statement.setString(2,user.getPassword());

      int rowInserted = AppConst.statement.executeUpdate();
      if(rowInserted > 0) {
          System.out.println("A new User is regestered Successfully !!");
      }
    }

}
