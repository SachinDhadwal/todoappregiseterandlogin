package main;

import CustomException.InvalidUserException;
import dao.TaskDao;
import dao.UserDao;
import daoImpl.*;
import model.Task;
import model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import static dao.UserDao.users;

public class MainDemo {
    private static final UserDao userDao = new UserDaoImpl();
    private static final TaskDao taskDao = new TaskDaoImpl();
    static Scanner in = new Scanner(System.in);
    static LogData logData = new LogData();
    static List<String> list = new ArrayList<>();
  static   Logger logger = Logger.getLogger(String.valueOf(MainDemo.class));

    public static void main(String[] args) throws InterruptedException {
//        System.out.println("1. Add a task \n ");
//        System.out.println("2. Update a task");
//        System.out.println("3. Search a task");
//        System.out.println("4. Delete a task");
//        System.out.println("5. Assign a task");
//        System.out.println("6. List all tasks");
//        System.out.println("7. List all assigned tasks");
//        System.out.println("Enter your choice: ");
        list.add("1. Add a task");
        list.add("2. Update a task");
        list.add("3. Search a task");
        list.add("4. Delete a task");
        list.add("5. Assign a task");
        list.add("6. List all tasks");
        list.add("7. List all assigned tasks");
        list.add("Enter your choice: ");





        taskDao.taskInitializer();
        do {
            System.out.println("Hello!");
            System.out.println("Press 1 if you are a client.");
            System.out.println("Press 2 if you are a visitor.");
            int option = in.nextInt();
            in.nextLine();
            if (option == 1) {
                Thread client = new Thread(MainDemo::client);
                client.start();
                client.join();

            } else if(option == 2){
                Thread visitor = new Thread(MainDemo::visitor);
                visitor.start();
                visitor.join();

            }else{
                System.out.println("Incorrect option. Try again!");
            }
            System.out.println("Do you want to go back to login menu? Press 0 to exit/ Any other key(1-9) to continue!");

        } while (in.nextInt() != 0);

    }

    static User loginMenu() throws SQLException {
        System.out.println("1. Login");
        System.out.println("2. Register");
        int choice = in.nextInt();
        in.nextLine();
        String name, password;
        User user = null;
        switch (choice) {
            case 1:
                System.out.println("Please enter name: ");
                name = in.nextLine();
                System.out.println("Please enter password: ");
                password = in.nextLine();
                user = userDao.isValidUser(name, password);
                Login  login = new Login();
                login.doLogin(name,password);
                break;

            case 2:
                System.out.println("Please enter name: ");
                name = in.nextLine();
                System.out.println("Please enter password: ");
                password = in.nextLine();
                user = new User(name, password);
                users.add(user);
                Register register = new Register();
                register.doRegister(name,password);
                break;
            default:
                System.out.println("Wrong choice!");
                System.exit(0);
        }
        return user;
    }

    static void visitor()  {
        do {
            User user = null;

            try {
                user = loginMenu();
            }catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (user == null)
                    throw new InvalidUserException("Unauthorized user");
                System.out.println("Do you want to list all assigned tasks?(yes/no)");
                String ans = in.nextLine();
                if (ans.equalsIgnoreCase("yes"))
                    userDao.listAssignedTasks(user.getName());
            } catch (InvalidUserException e) {
                System.out.println(e);
            }
            System.out.println("Do you want to continue with visitor menu? Press 0 to exit/ Any other key(1-9) to continue!");
        } while (in.nextInt() != 0);
    }

    static void client()  {

        do {
            User user = null;
            try {
                user = loginMenu();
            }
             catch (SQLException exception){
                exception.printStackTrace();
             }
            try {
                if (user == null)
                    throw new InvalidUserException("Unauthorized user");
                do {
                list.forEach(item -> System.out.println(item));
//                    System.out.println(list);
//                    for (Object o :list
//                         ) {
//                        System.out.println(o);
//                    }
                    int choice = in.nextInt();
                    switch (choice) {
                        case 1:
                            System.out.println("Enter task id: ");
                            int taskId = in.nextInt();
                            in.nextLine();
                            System.out.println("Enter task title: ");
                            String taskTitle = in.nextLine();
                            taskDao.add(taskId, taskTitle);
                            logData.storeData("Add Method Called");
                            logger.info("Hello this from log");
                            break;

                        case 2:
                            System.out.println("Enter task id: ");
                            taskId = in.nextInt();
                            in.nextLine();
                            taskDao.update(taskId);
                            logData.storeData("update method called");
                            break;

                        case 3:
                            System.out.println("Enter task id: ");
                            taskId = in.nextInt();
                            AppConst.scanner.nextLine();
                            Task task = taskDao.search(taskId);
                            logData.storeData("search method called");

                            if (task != null)
                                System.out.println("Task " + task.getTaskTitle() + " with task id " + taskId + " found!");
                            else
                                System.out.println("Task with task id " + taskId + " not found!");
                            break;

                        case 4:
                            System.out.println("Enter task id: ");
                            taskId = in.nextInt();
                            in.nextLine();
                            taskDao.delete(taskId);
                            logData.storeData("deleted method called");
                            break;

                        case 5:
                            System.out.println("Enter task id: ");
                            taskId = in.nextInt();
                            in.nextLine();
                            System.out.println("Enter username: ");
                            String username = in.nextLine();
                            taskDao.assign(taskId, username);
                            logData.storeData("assign method Method Called");
                            break;

                        case 6:
                            taskDao.listTasks();
                            logData.storeData("list task method called");
                            break;

                        case 7:
                            userDao.listAssignedTasks(user.getName());
                            logData.storeData("listAssign Method Called");
                            break;

                        default:
                            System.out.println("Wrong choice!");
                            logData.storeData("Invalid input");
                    }
                    System.out.println("Do you want to continue? Press 0 to exit/ Any other key(1-9) to continue!");

                } while (in.nextInt() != 0);
            } catch (InvalidUserException e) {
                System.out.println(e);
            }
            System.out.println("Do you want to continue to client login menu? Press 0 to exit/ Any other key(1-9) to continue!");
        } while (in.nextInt() != 0);

    }


}
